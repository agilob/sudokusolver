<a href="https://hosted.weblate.org/engage/sudokusolver/?utm_source=widget">
<img src="https://hosted.weblate.org/widgets/sudokusolver/-/svg-badge.svg" alt="Translation status" />
</a>

# SudokuSolver

SudokuSolver is an application created to help people play and solve Sudoku using their portable devices like phones, tables or smart watches with Android and built-in camera.

The application scans preview from a built-in camera, detects numbers with the help of an OCR engine and eventually solves the puzzle. My idea was to create an application that requires as little interaction from users as possible, works in real-time, without freezing the screen or stopping user from other activities, and has a user interface which is as simple as possible. When opened, the application detects cameras and starts built-in forward-facing camera if available, if not, font camera is launched. Camera preview is then modified by drawing square grids where user must calibrate Sudoku to fit into them. By clicking on the “Scan” button the device takes a picture and starts OCR detection. 
The application supports three different OCR methods which provide different OCR detection accuracy. The higher the accuracy, the slower the detection process. The default size of the grid and OCR engine can be selected in applications preferences. On start, Sudoku size is set to 3x3 and OCR engine to "Automatic".

SudokuSolver has features allowing easy modifications of detected digits, quick input to selected field and validation of detected and provided digits. The application supports saving current Sudoku to file, importing it and Solving. The solving algorithm works with all kinds of Sudokus which have the same number of rows and columns.

# Screenshots
![grid9x9](screenshots/grid9x9.png)
![grid9x9 solved](screenshots/grid9x9s.png)
![grid3x3](screenshots/grid3x3.png)
![grid3x3 solved](screenshots/grid3x3s.png)
[![piwik](https://agilob.net/piwik/piwik.php?idsite=5&rec=1)](https://agilob.net/) 