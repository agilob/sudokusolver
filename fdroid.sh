#! /usr/bin/bash
cp sudoku_solver/build/outputs/apk/sudoku_solver-release-*.*.apk /home/agilob/Projects/fdroid/repo
cd /home/agilob/Projects/fdroid
./fdroid update
./fdroid publish
rsync -a -e 'ssh -p 3022' --progress repo fdroid@agilob.net:~
