package net.agilob.sudokusolver.structures;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Field implements Parcelable, Serializable {

    private int x = -1;
    private int y = -1;
    private transient Range rangeX;
    private transient Range rangeY;
    /**
     * Bitmap is transient, won't be serialized, no need for this.
     */
    private transient Bitmap bitmap;
    private int number = 0;
    private boolean isValid = true;

    public static final Parcelable.Creator<Field> CREATOR = new Parcelable.Creator<Field>() {
        @Override
        public Field createFromParcel(Parcel in) {
            return new Field(in);
        }

        @Override
        public Field[] newArray(int size) {
            return new Field[size];
        }
    };

    public Field() {
        rangeX = new Range();
        rangeY = new Range();
    }

    public Field(int x, int y, Range rangeX, Range rangeY, Bitmap bitmap) {
        this.rangeY = rangeY;
        this.rangeX = rangeX;
        this.y = y;
        this.x = x;
        this.bitmap = bitmap;
    }

    Field(Parcel in) {
        x = in.readInt();
        y = in.readInt();
        rangeX = (Range) in.readValue(Range.class.getClassLoader());
        rangeY = (Range) in.readValue(Range.class.getClassLoader());
        number = in.readInt();
        isValid = in.readByte() != 0x00;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setIsValid(boolean valid) {
        this.isValid = valid;
    }

    public int x() {
        return x;
    }

    public void x(int x) {
        this.x = x;
    }

    public int y() {
        return y;
    }

    public void y(int y) {
        this.y = y;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(x);
        dest.writeInt(y);
        dest.writeValue(rangeX);
        dest.writeValue(rangeY);
        dest.writeInt(number);
        dest.writeByte((byte) (isValid ? 0x01 : 0x00));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "X: " + x + "  Y:" + y + "  x-x1: " + rangeX.toString() + "  y-y1: " + rangeY.toString();
    }
}
