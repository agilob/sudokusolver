package net.agilob.sudokusolver.structures;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Range implements Parcelable, Serializable {

    private int from = 0;
    private int to = 0;

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Range> CREATOR = new Parcelable.Creator<Range>() {
        @Override
        public Range createFromParcel(Parcel in) {
            return new Range(in);
        }

        @Override
        public Range[] newArray(int size) {
            return new Range[size];
        }
    };

    public Range() {
        this.from = 0;
        this.to = 0;
    }

    public Range(int from, int to) {
        this.from = from;
        this.to = to;
    }

    protected Range(Parcel in) {
        from = in.readInt();
        to = in.readInt();
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    @Override
    public String toString() {
        return "{ x : " + from + ", y : " + to + " }";
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(from);
        dest.writeInt(to);
    }

    @Override
    public int describeContents() {
        return 0;
    }

}
