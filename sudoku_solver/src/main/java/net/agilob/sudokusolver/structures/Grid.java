package net.agilob.sudokusolver.structures;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import net.agilob.sudokusolver.GridDetectionAsyncTask;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Grid implements Parcelable, Serializable {

    private static final String TAG = "GRID";

    private int size;
    private boolean isSolved = false;
    private List<List<Field>> fields;
    private String date;
    private transient Context context;

    public static final Parcelable.Creator<Grid> CREATOR = new Parcelable.Creator<Grid>() {
        @Override
        public Grid createFromParcel(Parcel in) {
            return new Grid(in);
        }

        @Override
        public Grid[] newArray(int size) {
            return new Grid[size];
        }
    };

    public Grid() {
        setDate();
    }

    public Grid(Context context, int gridSize) {
        this.context = context;
        setSize(gridSize);
        setDate();
    }

    public Grid(int gridSize) {
        setSize(gridSize);
        setDate();
    }

    protected Grid(Parcel in) {
        size = in.readInt();
        if (in.readByte() == 0x01) {
            fields = new ArrayList<>();
            in.readList(fields, Field.class.getClassLoader());
        } else {
            fields = null;
        }
        date = in.readString();
    }

    public Field getFieldAt(int x, int y) {
        return fields.get(x).get(y);
    }

    /**
     * Method used when setting filed by a user.
     * Sets number in a field, validates it, checks if the same number already exists in row/column.
     *
     * @param newField field to set
     * @param number   number to validate a field
     */
    public void inputField(Field newField, int number) {

        newField.setNumber(number);
        newField.setIsValid(alreadyExistsInLine(newField));

        if (newField.isValid()) {
            newField.setIsValid(isNumberValid(newField.getNumber()));
        }

        fields.get(newField.x()).set(newField.y(), newField);
    }

    /**
     * Method used while setting filed by automatic process like during scanning.
     *
     * @param newField
     */
    public void setField(Field newField) {

        runDetectorForField(newField);
        newField.setIsValid(alreadyExistsInLine(newField));

        if (newField.isValid()) {
            newField.setIsValid(isNumberValid(newField.getNumber()));
        }

        fields.get(newField.x()).set(newField.y(), newField);
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;

        fields = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            // add row:
            ArrayList<Field> list = new ArrayList<>();
            fields.add(list);
            for (int j = 0; j < size; j++) {
                // add a column:
                list.add(new Field(i, j, new Range(), new Range(), null));
            }
        }
    }

    public List<List<Field>> getFields() {
        return fields;
    }

    /**
     * Method checks if a number already exists in a column and row in current sudoku grid.
     * If so all matching fields are marked as invalid.
     *
     * @param f
     * @return
     */
    private boolean alreadyExistsInLine(Field f) {
        boolean valid = true;
        Field fieldRow;
        Field fieldColumn;

        for (int i = 0; i < size; i++) {

            fieldRow = getFieldAt(f.x(), i);
            fieldColumn = getFieldAt(i, f.y());

            if (fieldRow != null && f.getNumber() == fieldRow.getNumber()) {
                fieldRow.setIsValid(false);
                valid = false;
            }

            if (fieldColumn != null && f.getNumber() == fieldColumn.getNumber()) {
                fieldColumn.setIsValid(false);
                valid = false;
            }
        }

        return valid;
    }

    private void setDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        date = sdf.format(new Date());
    }

    /**
     * Method parses input looking for number, takes first number found in the string
     * checks if the number was greater than 9, if so, takes first digit.
     * ORC sometimes detects numbers with additional characters like ;-1/34
     * the method will find 1 and return it.
     * <p/>
     * It happened a few times OCR detected -1 or -4 so returned values is validated with Math.abs() for sure.
     *
     * @param detected Output from OCR.
     * @return first detected digit.
     */
    private static int getNumberFromDetectedString(String detected) {
        int parsed = 0;
        try {
            parsed = Integer.parseInt(detected);
            if (parsed > 9)
                parsed = parsed % 10; //often 1 is detected as 11, 7 as 77, 8 as 88 etc.

        } catch (NumberFormatException e) {
            Log.e(TAG, String.valueOf(e));
            try {
                String s = detected;
                Matcher matcher = Pattern.compile("\\d").matcher(s);
                matcher.find();

                int matched = Integer.parseInt(matcher.group());
                if (matched > 0 && matched <= 9)
                    parsed = matched;

            } catch (NumberFormatException | IllegalStateException ex) {
                //incorrectly detected number again...
                //no match
                Log.i(TAG, String.valueOf(ex));
            }
        }
        // just in case if a line, dot or mouse cursor was before number
        return Math.abs(parsed);
    }

    /**
     * Starts background thread for number in provided field, sets detected number and validates it.
     *
     * @param f Field to run OCR for.
     */
    private void runDetectorForField(Field f) {
        String detected = "";
        try {
            GridDetectionAsyncTask gdat = new GridDetectionAsyncTask();
            gdat.setContext(context);
            detected = gdat.execute(f.getBitmap()).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(TAG, String.valueOf(e));
        }

        if (!detected.isEmpty()) {
            int parsed = getNumberFromDetectedString(detected);
            f.setNumber(parsed);
            f.setIsValid(isNumberValid(parsed));
        }
    }

    /**
     * Number must be greater than 0 and lower than size of the grid to be valid.
     *
     * @param number
     * @return
     */
    public boolean isNumberValid(int number) {
        return number > 0 && number <= size;
    }

    public String getDate() {
        return date;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public boolean isSolved() {
        return isSolved;
    }

    public void setSolved(boolean solved) {
        this.isSolved = solved;
    }

    @Override
    /**
     * Converts fields in the grid to human readable format like:
     * 000|430|000|
     *
     * @return
     */
    public String toString() {
        String lines = "";
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {

                if (this.getFieldAt(i, j) == null)
                    lines += "0";
                else
                    lines += this.getFieldAt(i, j).getNumber();
            }
            lines += "|";
        }
        return lines;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(size);
        if (fields == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(fields);
        }
        dest.writeString(date);
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
