package net.agilob.sudokusolver;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.*;
import android.widget.Toast;

import net.agilob.sudokusolver.structures.Grid;

import java.io.File;
import java.io.Serializable;

public class MainActivity extends Activity {

    private CameraFragment mCamFragment;
    private DrawOnTop mDrawOnTop;
    private ProgressDialog mProgressDialog;
    private static final int progressBarType = 0;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checkIfFirstStart();

        setContentView(R.layout.activity_main);

        //init camera
        mCamFragment = CameraFragment.newInstance(false);
        getFragmentManager().beginTransaction().replace(R.id.container, mCamFragment).commit();

        //very slow camera loading can cause mdrawontop is null sometimes... or when camera is locked by other application
        if (mDrawOnTop == null) {
            mDrawOnTop = new DrawOnTop(this, mCamFragment);
            addContentView(mDrawOnTop, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        }
    }

    /**
     * Should return something like:
     * /storage/emulated/0/Android/data/net.agilob.sudokusolver/files/download/
     *
     * @return
     */
    private String getDataPath() {
        return getExternalFilesDir(DOWNLOAD_SERVICE).getAbsolutePath() + "/";
    }

    /**
     * Should return something like:
     * /storage/emulated/0/Android/data/net.agilob.sudokusolver/files/download/tessdata/
     *
     * @return
     */
    private String getTessDataPath() {
        return getDataPath() + "tessdata/";
    }

    /**
     * Method checks if traineddata exists in specified location. if not, this is the first time application was launched.
     * it will be necessary to download traineddata.
     */
    private void checkIfFirstStart() {
        final File f = new File(getTessDataPath());

        if (!f.exists() && !f.isDirectory()) {
            handler = new Handler();

            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (which == DialogInterface.BUTTON_POSITIVE) {
                        Toast.makeText(getApplicationContext(),
                                getString(R.string.downloading_ocr_data), Toast.LENGTH_SHORT).show();
                        new Downloader().execute(getString(R.string.tessDownloadResource), f.toString());
                    } else {
                        finish();
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setMessage(getString(R.string.ask_to_download_data)).setPositiveButton(getString(android.R.string.yes), dialogClickListener)
                    .setNegativeButton(getString(android.R.string.no), dialogClickListener).show();
        }
    }


    /**
     * Show progress bar dialog.
     *
     * @param id type of the dialog.
     */
    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == 0) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            mProgressDialog.setMessage(getString(R.string.downloading_traineddata));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(11);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
            return mProgressDialog;
        }
        return null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        new MenuInflater(this).inflate(R.menu.activity_main, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        Intent intent = new Intent();

        switch (item.getItemId()) {
            case R.id.settings:
                intent.setClass(this, SettingsActivity.class);
                startActivity(intent);
                return true;

            case R.id.load_saved_sudoku:
                intent.setClass(this, SudokuManager.class);
                intent.putExtra("path", getDataPath());
                startActivityForResult(intent, 1);
                return true;

            case R.id.save_current_sudoku:
                intent.setClass(this, SudokuManager.class);
                intent.putExtra("path", getDataPath());
                intent.putExtra("grid", (Serializable) mDrawOnTop.getSudokuGrid());
                startActivity(intent);
                return true;

            default:
                return super.onMenuItemSelected(featureId, item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK) {
            mDrawOnTop.setNewGrid((Grid) data.getSerializableExtra("grid"));
        }
    }


    public void onSolveAll(View v) {
        mDrawOnTop.solveGrid();
    }

    public void changeGridSize(View v) {
        if (v.getId() == R.id.button3by3) {
            mDrawOnTop.setGridSize(3);
        } else {
            mDrawOnTop.setGridSize(9);
        }
    }

    public void onScan(View v) {
        mDrawOnTop.scanAll();
    }

    private class Downloader extends TessDownloader {
        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progressBarType);
        }

        /**
         * Updating progress bar
         */
        @Override
        protected void onProgressUpdate(Integer... progress) {
            // setting progress percentage
            mProgressDialog.setProgress(progress[0]);
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         */
        @Override
        protected void onPostExecute(Long status) {
            // dismiss the dialog after the file was downloaded
            dismissDialog(progressBarType);
        }

        protected void exceptionOccured(final String msg) {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(MainActivity.this);
                            dlgAlert.setMessage(getString(R.string.unable_to_download_data) + msg);
                            dlgAlert.setTitle(getString(R.string.problem_downloading_data));
                            dlgAlert.setCancelable(false);
                            dlgAlert.setPositiveButton(getString(R.string.quit), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    finish();
                                }
                            });
                            dlgAlert.create();
                            dlgAlert.show();
                        }
                    });
                }
            };
            Thread myThread = new Thread(runnable);
            myThread.start();
        }
    }
}
