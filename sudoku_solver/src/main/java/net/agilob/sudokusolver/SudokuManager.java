package net.agilob.sudokusolver;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import net.agilob.sudokusolver.structures.Grid;

import java.io.*;
import java.util.ArrayList;

public class SudokuManager extends ListActivity {

    private static final String TAG = "SudokuManager";
    private ListView mListView;
    private ArrayAdapter listAdapter;
    private ArrayList<Grid> grids;
    private String path;
    private File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_sudoku);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            path = extras.getString("path") + "sudokus.ser";
            file = new File(path);

            Grid g = (Grid) extras.get("grid");
            if (g != null)
                saveGrid(g);
        }

        mListView = getListView();
        grids = loadGrids();

        ArrayList<String> list = new ArrayList<>();
        for (Grid grid : grids) {
            list.add(grid.getDate());
        }

        listAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, list);
        mListView.setAdapter(listAdapter);
    }

    private void saveGrid(Grid g) {
        grids = loadGrids();
        grids.add(0, g);

        try {
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(grids);
            os.close();
            fos.close();
        } catch (IOException e) {
            Log.e(TAG, String.valueOf(e));
            Toast.makeText(getApplicationContext(), R.string.failed_to_save_sudoku, Toast.LENGTH_SHORT).show();
            return; //quit method to avoid showing second toast
        }
        Toast.makeText(getApplicationContext(), R.string.saved_sudoku + g.getDate(), Toast.LENGTH_SHORT).show();
    }

    private ArrayList<Grid> loadGrids() {
        ArrayList<Grid> gArray = new ArrayList<>();
        try {
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream is = new ObjectInputStream(fis);
            gArray = (ArrayList<Grid>) is.readObject();
            is.close();
            fis.close();
        } catch (IOException | ClassNotFoundException e) {
            Log.e(TAG, String.valueOf(e));
        }

        return gArray;
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("grid", (Serializable) grids.get(position));
        setResult(RESULT_OK, returnIntent);
        finish();
    }

}
