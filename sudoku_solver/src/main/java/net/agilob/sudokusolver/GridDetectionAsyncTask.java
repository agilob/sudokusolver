package net.agilob.sudokusolver;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import com.googlecode.tesseract.android.TessBaseAPI;

public class GridDetectionAsyncTask extends AsyncTask<Bitmap, String, String> {

    private final String TAG = "GDAT";

    // very interesting behaviour about this static attribute
    // before is was static, scanning one btimap took ~1.8sec
    // after it's static it takes less than 0.5sec to scan 81 bitmaps!
    private static TessBaseAPI baseApi;
    private static String path;
    // lang = for which the language data exists, using only "eng"
    private final String lang = "eng";
    // required to access preferencemanager
    private Context context;



    /**
     * Gets selected preferences and starts OCR on a bitmap.
     * @param bitmap
     * @return
     */
    @Override
    protected String doInBackground(Bitmap... bitmap) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String detected = runDetector(Integer.parseInt(prefs.getString("ocr_engine", "3")), bitmap[0]);

        return detected;
    }

    /**
     * Setups OCR engine/mode. Checks if tessbaseapi was initialized to void doing it over and over for all fields.
     * This variable is static to avoid reinitialization, it can be safely shared between threads.
     * @param ocrEngine
     * @param bitmap
     * @return
     */
    private String runDetector(int ocrEngine, Bitmap bitmap) {
        Log.i(TAG, "Using detector: " + ocrEngine);
        if (baseApi == null) {
            baseApi = new TessBaseAPI();
            baseApi.setPageSegMode(TessBaseAPI.PageSegMode.PSM_SINGLE_CHAR);  //find only one character
            baseApi.setVariable(TessBaseAPI.VAR_CHAR_WHITELIST, "123456789"); //whitelist, concentrate on those characters
            path = context.getExternalFilesDir(context.DOWNLOAD_SERVICE).getAbsolutePath() + "/";
        }

        baseApi.setImage(bitmap);
        baseApi.init(path, lang, ocrEngine);
        return baseApi.getUTF8Text();
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
