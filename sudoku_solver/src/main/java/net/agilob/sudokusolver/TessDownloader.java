package net.agilob.sudokusolver;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

class TessDownloader extends AsyncTask<String, Integer, Long> {

    private static final String TAG = "TESSDownloader";

    private ZipEntry zipEntry;

    /**
     * http://www.androidsnippets.com/download-an-http-file-to-sdcard-with-progress-notification
     *
     * @param uris
     *         First element in the array is where the remote file is located. Second
     *         elements says where the file will be downloaded.
     */
    @Override
    protected Long doInBackground(String... uris) {
        try {
            // IF you are unzipping a zipped file save under some URL in remote server
            URL finalUrl = new URL(uris[0]);

            URLConnection urlConnection;
            urlConnection = finalUrl.openConnection();

            ZipInputStream zipInputStream = new ZipInputStream(urlConnection.getInputStream());

            /*
             * Iterate over all the files and folders
             */
            int i = 0;
            for(zipEntry = zipInputStream.getNextEntry();
                zipEntry != null;
                zipEntry = zipInputStream.getNextEntry(), i++) {

                Log.i(TAG, "Extracting: " + zipEntry.getName() + "...");

                /*
                 * Extracted file will be saved with same file name that in
                 * zipped folder.
                 */
                String innerFileName = uris[1] + File.separator + zipEntry.getName();
                File innerFile = new File(innerFileName);

                /*
                 * Checking for pre-existence of the file and taking
                 * necessary actions
                 */
                if(innerFile.exists()) {
                    innerFile.delete();
                }

                new File(uris[1]).mkdirs();
                new File(innerFileName).createNewFile();

                FileOutputStream outputStream = new FileOutputStream(innerFileName);
                final int BUFFERSIZE = 4096;

                /*
                 * Get the buffered output stream..
                 */
                BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream,
                                                                                     BUFFERSIZE);
                /*
                 * Write into the file's buffered output stream ,..
                 */
                int count;
                byte[] buffer = new byte[BUFFERSIZE];
                while((count = zipInputStream.read(buffer, 0,
                                                   BUFFERSIZE)) != -1) {
                    bufferedOutputStream.write(buffer, 0, count);
                }

                bufferedOutputStream.flush();
                bufferedOutputStream.close();

                zipInputStream.closeEntry();
                publishProgress(i);
            }
            //download finished, close stream
            zipInputStream.close();
        } catch(IOException e) {
            Log.i(TAG, "Exception occurred: " + e);
        }

        return null;
    }
}
