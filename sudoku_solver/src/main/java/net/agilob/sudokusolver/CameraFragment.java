package net.agilob.sudokusolver;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.widget.Toast;
import com.commonsware.cwac.camera.PictureTransaction;
import com.commonsware.cwac.camera.SimpleCameraHost;

/**
 * Code adapted from https://github.com/commonsguy/cwac-camera/tree/master/demo
 */
public class CameraFragment extends com.commonsware.cwac.camera.CameraFragment {

    private static final String KEY_USE_FFC = "com.commonsware.cwac.camera.demo.USE_FFC";
    private static final String TAG = "CameraFragment";
    private CameraHost mCamHost;
    private DrawOnTop drawOnTop;

    static CameraFragment newInstance(boolean useFFC) {
        CameraFragment f = new CameraFragment();

        Bundle args = new Bundle();

        args.putBoolean(KEY_USE_FFC, useFFC);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        setHasOptionsMenu(true);
        mCamHost = new CameraHost(getActivity());
        this.setHost(mCamHost);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View cameraView = super.onCreateView(inflater, container, savedInstanceState);
        View results = inflater.inflate(R.layout.fragment, container, false);

        ((ViewGroup) results.findViewById(R.id.camera)).addView(cameraView);
        autoFocus();

        return results;
    }

    public void waitForBitmap(DrawOnTop drawOnTop) {
        this.drawOnTop = drawOnTop;

        // must be called in case user taps screen too quickly and camera
        // doesn't have time to restart preview automatically
        restartPreview();

        try {
            takePicture();
        } catch (IllegalStateException e) {
            Toast.makeText(getActivity().getApplicationContext(), "Failed to take picture", Toast.LENGTH_SHORT).show();
            Log.e(TAG, String.valueOf(e));
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().invalidateOptionsMenu();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.camera, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.autofocus){
                autoFocus();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    class CameraHost extends SimpleCameraHost {
        public CameraHost(Context ctxt) {
            super(ctxt);
        }

        @Override
        public boolean useFrontFacingCamera() {
            return getArguments().getBoolean(KEY_USE_FFC);
        }

        /**
         * Method indicates if after taking picture bitmap is frozen or next frame shown.
         *
         * @return Enable showing preview, must be false for this project.
         */
        @Override
        public boolean useSingleShotMode() {
            return false;
        }

        @Override
        public void saveImage(PictureTransaction xact, byte[] image) {
            drawOnTop.setBitmap(BitmapFactory.decodeByteArray(image, 0, image.length));
        }

        @Override
        public void onCameraFail(CameraHost.FailureReason reason) {
            Toast.makeText(getActivity(), "Sorry, but you cannot use the camera now!", Toast.LENGTH_LONG).show();
            super.onCameraFail(reason);
        }

        @Override
        public boolean useFullBleedPreview() {
            return true;
        }

    }
}