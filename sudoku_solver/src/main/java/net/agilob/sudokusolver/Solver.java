package net.agilob.sudokusolver;

import android.os.AsyncTask;
import android.util.Log;
import net.agilob.sudokusolver.structures.Grid;

/**
 * The solver uses a technique called backtracking to come up with valid solution for a puzzle.
 * The technique is implemented to search for all possible solutions for a puzzle.
 */
class Solver extends AsyncTask<Grid, Integer, Boolean> {

    private Grid g;
    private int gridSize;
    private static final String TAG = "Solver";
    private int[][] matrix;

    @Override
    protected Boolean doInBackground(Grid... grids) {

        gridSize = grids[0].getSize();
        g = grids[0];

        copyGridToMatrix();

        boolean isSolved = solveSudoku();
        if (isSolved) {
            g.getFields().clear();
            g.setSize(gridSize);
            copyMatrixToGrid();
            g.setSolved(true);
        }
        return isSolved;
    }


    private void copyGridToMatrix() {
        matrix = new int[gridSize][gridSize];

        for (int i = 0; i < gridSize; i++) {
            for (int j = 0; j < gridSize; j++) {
                if (g.getFieldAt(i, j).isValid())
                    matrix[j][i] = g.getFieldAt(i, j).getNumber();
            }
        }

    }

    private void copyMatrixToGrid() {
        for (int row = 0; row < gridSize; row++) {
            for (int col = 0; col < gridSize; col++) {
                g.getFieldAt(row, col).setNumber(matrix[col][row]);
                g.getFieldAt(row, col).setIsValid(g.isNumberValid(matrix[col][row]));
            }
        }
    }

    private boolean solveSudoku() {
        int row, col;

        int[] unassigned = findUnassignedField();

        if ((unassigned[0] == gridSize + 1) && (unassigned[1] == gridSize + 1))
            return true; //solved
        else {
            row = unassigned[0];
            col = unassigned[1];
        }

        // consider digits from 1 to N, N is size of the grid
        for (int num = 1; num <= gridSize; num++) {
            if (isSafe(row, col, num)) {
                //number is valid, set it in the cell
                matrix[row][col] = num;

                //no more fields to solve
                if (solveSudoku())
                    return true;

                // failure, cancel previous number by setting value to 0
                matrix[row][col] = 0;
            }
        }
        // if false, solving will continue
        return false; //triggers backtracking
    }


    /**
     * Finds first unassigned field
     *
     * @return position x,y of the field.
     */
    private int[] findUnassignedField() {
        int[] unassigned = {gridSize + 1, gridSize + 1};

        for (int row = 0; row < gridSize; row++)
            for (int col = 0; col < gridSize; col++)
                if (matrix[row][col] == 0) { //field empty
                    unassigned[0] = row;
                    unassigned[1] = col;
                }
        return unassigned;
    }

    /**
     * Checks if a number already exists in the row. If doesn't the number can be safely placed here.
     *
     * @param row
     * @param num
     * @return
     */
    private boolean existsInRow(int row, int num) {
        for (int col = 0; col < gridSize; col++)
            if (matrix[row][col] == num)
                return true;
        return false;
    }

    /**
     * Checks if a number already exists in the column. If doesn't the number can be safely placed here.
     *
     * @param col
     * @param num
     * @return
     */
    private boolean existsInCol(int col, int num) {
        for (int row = 0; row < gridSize; row++)
            if (matrix[row][col] == num)
                return true;
        return false;
    }

    /**
     * Checks if a number already exists in the box.
     */
    private boolean existsInBox(int boxStartRow, int boxStartCol, int num) {
        if (gridSize == 3)
            return true;

        for (int row = 0; row < 3; row++)
            for (int col = 0; col < 3; col++)
                if (matrix[row + boxStartRow][col + boxStartCol] == num)
                    return true;
        return false;
    }

    /**
     * Checks if a number if valid for a row, column and box
     */
    private boolean isSafe(int row, int col, int num) {
    /*
        checks if current number exists in the row, column and box
     */
        return !existsInRow(row, num) &&
                !existsInCol(col, num) &&
                (gridSize == 3 || !existsInBox(row - row % 3, col - col % 3, num));
    }

    /**
     * Prints the grid in human readable format
     */
    private void printGrid() {
        String line;
        for (int row = 0; row < gridSize; row++) {
            line = "";
            for (int col = 0; col < gridSize; col++)
                line += Integer.toString(matrix[row][col]);
            Log.i(TAG, line);
        }
    }

}
