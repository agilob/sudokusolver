package net.agilob.sudokusolver;

import android.app.Application;

import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;
import org.acra.sender.HttpSender;

@ReportsCrashes(
        formUri = "http://agilob.net/projects/ss/reporter.php",
        httpMethod = HttpSender.Method.POST
)
public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        ACRA.init(this);
    }
}
