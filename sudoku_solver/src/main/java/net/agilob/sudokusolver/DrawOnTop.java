package net.agilob.sudokusolver;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.Point;
import android.preference.PreferenceManager;
import android.support.v4.view.GestureDetectorCompat;
import android.text.InputType;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import net.agilob.jhlabs.filter.MedianFilter;
import net.agilob.jhlabs.filter.ThresholdFilter;
import net.agilob.jhlabs.filter.util.AndroidUtils;
import net.agilob.sudokusolver.structures.Field;
import net.agilob.sudokusolver.structures.Grid;
import net.agilob.sudokusolver.structures.Range;

import java.util.concurrent.ExecutionException;

class DrawOnTop extends View implements
        GestureDetector.OnGestureListener,
        GestureDetector.OnDoubleTapListener {

    private static final String TAG = "DrawOnTop";
    private final GestureDetectorCompat mDetector;
    private final CameraFragment mCameraFragment;
    private final Context context;

    private int startX;
    private int startY;
    private int fieldWidth;
    private int canvasWidth;
    private int canvasHight;

    private Bitmap mRgba;
    private int gridSize;
    private final Paint paint;
    private final Paint fontPaint;
    private Bitmap field;
    private final Point point;
    private Grid sudokuGrid;
    private int[] array;
    private boolean afterTouch = false;
    private boolean afterScanAll = false;
    private boolean doubleTap = false;

    public DrawOnTop(Context context, CameraFragment mCamFragment) {
        super(context);
        this.context = context;
        this.mCameraFragment = mCamFragment;
        this.mRgba = null;

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        this.setGridSize(Integer.parseInt(prefs.getString("default_sudoku_size", "9")));
        mDetector = new GestureDetectorCompat(context, this);
        // Set the gesture detector as the double tap listener.
        mDetector.setOnDoubleTapListener(this);
        point = new Point();

        paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.WHITE);

        fontPaint = new Paint();
        fontPaint.setColor(Color.WHITE);
    }

    /**
     * Computes start/end range for given Y-position in the grid
     *
     * @param y
     * @return
     */
    private Range getRangeYFor(int y) {
        return new Range(startY + y * fieldWidth,       //y
                         startY + y * fieldWidth + fieldWidth);  //y1
    }

    /**
     * Computes start/end range for given X-position in the grid
     *
     * @param x
     * @return
     */
    private Range getRangeXFor(int x) {
        return new Range(startX + x * fieldWidth,      //x
                         startX + x * fieldWidth + fieldWidth); //x1
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvasWidth = canvas.getWidth();
        canvasHight = canvas.getHeight();

        if (field != null) {
            canvas.drawBitmap(field, 0, (canvasHight / 2) - (fieldWidth / 2), paint);
        }
        //draws all fields
        for (int i = 0; i < gridSize; i++) {
            for (int j = 0; j < gridSize; j++) {
                if (sudokuGrid.getFieldAt(i, j).getNumber() != 0) {
                    if (sudokuGrid.getFieldAt(i, j).isValid())
                        fontPaint.setColor(Color.WHITE);
                    else
                        fontPaint.setColor(Color.RED);

                    //if solved than use green pen for all
                    if (sudokuGrid.isSolved())
                        fontPaint.setColor(Color.GREEN);

                    // 0.3 and 0.6 are font dependent but those values do the job of positioning
                    // text in the middle of a field
                    // getting new range for each field, because after solving sudoku, a field doesn't have
                    // range values
                    canvas.drawText(
                            String.valueOf(sudokuGrid.getFieldAt(i, j).getNumber()),
                            (int) (getRangeXFor(sudokuGrid.getFieldAt(i, j).x()).getFrom() + fieldWidth * 0.3), //left
                            (int) (getRangeYFor(sudokuGrid.getFieldAt(i, j).y()).getFrom() + fieldWidth * 0.6), //top
                            fontPaint);

                }
            }
        }

        //
        paint.setStrokeWidth(2);
        //the first and the last vertical lines are wider, like bolding.
        if (isPortrait()) {
            startX = 0;
            startY = (canvasHight - canvasWidth) / 2;
            fieldWidth = canvasWidth / gridSize;

            //draws first left line
            canvas.drawLine(
                    startX, startY,
                    startX + canvasWidth, startY,
                    paint);
            //draws last (right) line
            canvas.drawLine(
                    startX, startY + fieldWidth * gridSize,
                    startX + canvasWidth, startY + canvasWidth,
                    paint);
        } else {
            startX = (canvasWidth - canvasHight) / 2;
            startY = 0;
            fieldWidth = canvasHight / gridSize;

            canvas.drawLine(
                    startX, startY,
                    startX, startY + canvasHight,
                    paint);
            canvas.drawLine(
                    startX + (fieldWidth * gridSize), startY,
                    startX + (fieldWidth * gridSize), startY + canvasHight,
                    paint);
        }

        paint.setStrokeWidth(1);
        //draws all other (middle) lines
        for (int i = 1; i < gridSize; i++) {
            //vertical lines
            canvas.drawLine(
                    startX, startY + fieldWidth * i,
                    startX + (fieldWidth * gridSize), startY + fieldWidth * i,
                    paint
            );
            // horizontal lines
            canvas.drawLine(
                    startX + (fieldWidth * i), startY,
                    startX + (fieldWidth * i), startY + fieldWidth * gridSize,
                    paint
            );
        }

        invalidate();
        super.onDraw(canvas);
    }


    /**
     * detect which field was touched
     * @return
     */
    private int[] detectGrid() {
        Log.i(TAG, "detectGrid");

        int gridX = 0;
        int gridY = 0;

        for (int i = 0; i < gridSize; i++) {
            if (point.x > startX + fieldWidth * i)
                gridX = i;

            if (point.y > startY + fieldWidth * i)
                gridY = i;
        }

        return new int[]{gridX, gridY};
    }

    private boolean isPortrait() {
        int orientation = context.getResources().getConfiguration().orientation;
        return orientation == Configuration.ORIENTATION_PORTRAIT;
    }

    public void setGridSize(int gridSize) {
        Log.i(TAG, "setGridSize");
        afterTouch = false;
        sudokuGrid = new Grid(context, gridSize);

        this.gridSize = gridSize;
    }

    public Grid getSudokuGrid() {
        Log.i(TAG, "getSudokuGrid");

        return sudokuGrid;
    }

    public void getGridBitmap() {
        field = Bitmap.createBitmap(mRgba, startX, 0, fieldWidth * 3, fieldWidth * 3);
    }

    public void setBitmap(Bitmap bitmap) {
        Log.i(TAG, "setBitmap");

        Bitmap scaledBitmap = Bitmap.createScaledBitmap(
                bitmap,
                canvasWidth,
                canvasHight,
                true);

        scaledBitmap = Bitmap.createBitmap(scaledBitmap,
                0, (canvasHight / 3) / 2,
                scaledBitmap.getWidth(),
                scaledBitmap.getHeight() - (canvasHight / 3)
        );

        mRgba = Bitmap.createScaledBitmap(scaledBitmap, canvasWidth, canvasHight, true);

        if (afterTouch) {
            Log.i(TAG, "afterTouch");

            sudokuGrid.setField(new Field(
                    array[0], array[1],
                    getRangeXFor(array[0]),
                    getRangeYFor(array[1]),
                    getBitmapFor(array[0], array[1])));
            afterTouch = false;
        }

        if (afterScanAll) {
            Log.i(TAG, "afterScanAll");

            sudokuGrid = new Grid(context, gridSize);

            for (int x = 0; x < gridSize; x++) {
                for (int y = 0; y < gridSize; y++) {
                    sudokuGrid.setField(new Field(x, y,
                            getRangeXFor(x),
                            getRangeYFor(y),
                            getBitmapFor(x, y)));
                }
            }

            afterScanAll = false;
        } //after scan all

    }//set bitmap


    /**
     * For given parameters x,y method find range x1-x2, y1-y2 and creates subimage.
     * Subimage is filtered from noise and converted to grayscale.
     *
     * @param x
     * @param y
     * @return
     */
    private Bitmap getBitmapFor(int x, int y) {
        field = Bitmap.createBitmap(mRgba,
                startX + (x * fieldWidth),
                startY + (y * fieldWidth),
                fieldWidth, fieldWidth);

//        field = removeNoise(field); //remove noise
        field = createGrayScaleBitmap(field); //convert to grayscale
//        field = removeNoise(field); //remove noise
        //field = thresholdFilter(field);

        return field;
    }

    /**
     * for a given bitmap searches for a color and replaces it with another color.
     * Was used with converting blue to black, to convert pen-written number to black.
     *
     * @param src
     * @param fromColor
     * @param targetColor
     * @return
     */
    public Bitmap replaceColor(Bitmap src, int fromColor, int targetColor) {
        if (src == null) {
            return null;
        }

        int[] pixels = new int[fieldWidth * fieldWidth];
        //get pixels
        src.getPixels(pixels, 0, fieldWidth, 0, 0, fieldWidth, fieldWidth);

        for (int x = 0; x < pixels.length; ++x) {
            pixels[x] = (pixels[x] == fromColor) ? targetColor : pixels[x];
        }

        // create result bitmap output
        Bitmap result = Bitmap.createBitmap(fieldWidth, fieldWidth, src.getConfig());
        //set pixels
        result.setPixels(pixels, 0, fieldWidth, 0, 0, fieldWidth, fieldWidth);

        return result;
    }

    /**
     * converts bitmap to grayscale by removing saturation.
     *
     * @param bmp
     * @return
     */
    private Bitmap createGrayScaleBitmap(Bitmap bmp) {
        Bitmap bmpGrayscale = Bitmap.createBitmap(fieldWidth, fieldWidth, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmpGrayscale);
        Paint tmpPaint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0); //0 for grayscale
        ColorMatrixColorFilter cmcf = new ColorMatrixColorFilter(cm);
        tmpPaint.setColorFilter(cmcf);
        c.drawBitmap(bmp, 0, 0, tmpPaint);

        return bmpGrayscale;
    }

    /**
     * removes noise from bitmap, method uses JHlabs library.
     *
     * @param srcBitmap
     * @return
     */
    public Bitmap removeNoise(Bitmap srcBitmap) {
        Log.i(TAG, "removeNoise");

        //in some cases median filter is better than ReduceNoiseFilter, but I coundn't
        // decide between them, medianfilter is faster.
        MedianFilter filter = new MedianFilter();
        //ReduceNoiseFilter filter = new ReduceNoiseFilter();

        //Change int Array into a bitmap
        int[] src = AndroidUtils.bitmapToIntArray(srcBitmap);
        //Applies a filter.

        int[] result = filter.filter(src, fieldWidth, fieldWidth);

        //Change the Bitmap int Array (Supports only ARGB_8888)
        return Bitmap.createBitmap(result, fieldWidth, fieldWidth, Bitmap.Config.ARGB_8888);
    }

    public Bitmap thresholdFilter(Bitmap srcBitmap) {
        ThresholdFilter filter = new ThresholdFilter();

        //Change int Array into a bitmap
        int[] src = AndroidUtils.bitmapToIntArray(srcBitmap);
        //Applies a filter.
        int[] result = filter.filter(src, fieldWidth, fieldWidth);

        //Change the Bitmap int Array (Supports only ARGB_8888)
        return Bitmap.createBitmap(result, fieldWidth, fieldWidth, Bitmap.Config.ARGB_8888);
    }

    public void scanAll() {
        afterTouch = false;

        mCameraFragment.waitForBitmap(this);
        afterScanAll = true;
    }

    /**
     * Delete selected field.
     *
     * @param motionEvent
     */
    @Override
    public void onLongPress(MotionEvent motionEvent) {
        if (afterTouch) {
            afterTouch = true;
            array = detectGrid();

            try {
                sudokuGrid.getFieldAt(array[0], array[1]).setNumber(0);
                sudokuGrid.setSolved(false);
            } catch (NullPointerException e) {
                Log.e(TAG, e.toString());
                // this happens if there is no field at specified position or field has 0 as detected number
                // which means, field should not be drawn on canvas
            }
        }
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
        Log.i(TAG, "onSingleTapConfirmed");
        mCameraFragment.waitForBitmap(this);
        array = detectGrid();

        return true;
    }

    /**
     * when doubletap is detected user will see native input field with numeric keyboard.
     * the number will be validated and added to the grid
     *
     * @param motionEvent
     * @return
     */
    @Override
    public boolean onDoubleTapEvent(MotionEvent motionEvent) {
        Log.i(TAG, "onDoubleTapEvent");

        if (doubleTap) {
            doubleTap = false;
            return false;
        }
        array = detectGrid();

        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(R.string.input_number);

        // Set an EditText view to get user input
        final EditText input = new EditText(context);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        alert.setView(input);

        alert.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if (input.getText().toString().isEmpty())
                    //user canceled input
                    return;
                int value = Integer.parseInt(input.getText().toString());
                if (!isNumberValid(value))
                    Toast.makeText(getContext(), R.string.invalid_number, Toast.LENGTH_SHORT).show();

                sudokuGrid.inputField(new Field(array[0], array[1], getRangeXFor(array[0]), getRangeYFor(array[1]), null), value);
            }
        });

        alert.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //Canceled.
            }
        });

        alert.show();

        return false;
    }

    @Override
    /**
     * if returned value is true, touch event will be passed to MainActivity to fire onClickListener
     * on proper buttons, if false, MainActivity will not be notified about the click. It's default Activty behaviour.
     * Returning false will fire others methods like onSingleTapUp, onLongPress etc, which are defined in this class.
     */
    public boolean onTouchEvent(MotionEvent event) {
        Log.i(TAG, "onTouchEvent");

        fontPaint.setTextSize((int) (fieldWidth * 0.75));
        if (isPortrait()) {
            point.x = (int) event.getX();
            point.y = (int) event.getY();
        } else {
            point.x = (int) event.getY();
            point.y = (int) event.getX();
        }

        //calls double tap detector
        this.mDetector.onTouchEvent(event);

        //recycle mrgba before we got new bitmap here
        if (mRgba != null) {
            mRgba.recycle();
            mRgba = null;
        }

        boolean returned = point.x > startX && point.x < startX + gridSize * fieldWidth;

        Log.i(TAG, "onTouchEvent: " + returned);

        if (returned)
            afterTouch = true;

        return returned;
    }

    @Override
    public boolean onDoubleTap(MotionEvent motionEvent) {
        Log.i(TAG, "onDoubleTap");

        doubleTap = true;
        return false;
    }

    /**
     * Resets grid.
     *
     * @param newGrid
     */
    public void setNewGrid(Grid newGrid) {
        Log.i(TAG, "setNewGrid");

        this.sudokuGrid = newGrid;
        this.sudokuGrid.setContext(context);
        this.gridSize = this.sudokuGrid.getSize();

        if (isPortrait())
            fieldWidth = canvasWidth / gridSize;
        else
            fieldWidth = canvasHight / gridSize;

        fontPaint.setTextSize((int) (fieldWidth * 0.75));
    }

    private boolean isNumberValid(int number) {
        Log.i(TAG, "isNumberValid");

        return sudokuGrid.isNumberValid(number);
    }

    /**
     * try solving the grid and show popup message after solving/not solving.
     */
    public void solveGrid() {
        Log.i(TAG, "solve grid");
        afterTouch = false;

        try {
            Log.d(TAG, sudokuGrid.toString());
            if (new Solver().execute(sudokuGrid).get() == true) {
                Toast.makeText(context, R.string.sudoku_solved, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, R.string.impossible_to_solve, Toast.LENGTH_SHORT).show();
            }
        } catch (InterruptedException | ExecutionException e) {
            Log.e(TAG, String.valueOf(e));
        }
    }

    @Override
    public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY) {
        return true;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return true;
    }

    @Override
    public boolean onDown(MotionEvent motionEvent) {
        return true;
    }

    @Override
    public void onShowPress(MotionEvent motionEvent) {
        // inherited method, never used, no point in it.
    }

    @Override
    public boolean onSingleTapUp(MotionEvent event) {
        return true;
    }
}
