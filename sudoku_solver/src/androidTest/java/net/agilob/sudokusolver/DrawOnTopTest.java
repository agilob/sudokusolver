package net.agilob.sudokusolver;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.test.InstrumentationTestCase;
import android.util.Log;
import net.agilob.sudokusolver.structures.Field;
import net.agilob.sudokusolver.structures.Grid;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutionException;

public class DrawOnTopTest extends InstrumentationTestCase {

    private static final String TAG = "DrawOnTopTest";
    private Grid grid;
    private Field field;
    private int size = 3;
    private Context mContext;
    private Resources res;

    protected void setUp() {
        mContext = getInstrumentation().getTargetContext();
        res = getInstrumentation().getTargetContext().getResources();
        testDownloader();
        grid = new Grid(mContext, size);
    }


    private Bitmap loadBitmap(int resource) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        return BitmapFactory.decodeResource(res, resource, options);
    }

    /**
     * Of course this test is device dependent.
     */
    public void testOCRPerformance() {
        // Get current time
        long start = System.currentTimeMillis();

        long elapsedTimeMillis = System.currentTimeMillis() - start;

        // ten test cases
        testDetectOne();
        testDetectTwo();
        testDetectThree();
        testDetectFive();
        testDetectFour();
        testDetectSix();
        testDetectSeven();
        testDetectEight();
        testDetectNine();
        testDetectEmpty();

        float elapsedTimeSec = elapsedTimeMillis / 1000F;
        Log.i(TAG, "Time elapsed to take 10 OCRs: " + elapsedTimeSec);
        assertTrue(elapsedTimeSec < 0.5);
    }

    private boolean tessDataDirExists() {
        File f = new File(mContext.getExternalFilesDir(Context.DOWNLOAD_SERVICE).getAbsolutePath() + "/tessdata/");

        return f.exists() && f.isDirectory();
    }

    public void testDownloader() {
        File f = new File(mContext.getExternalFilesDir(Context.DOWNLOAD_SERVICE).getAbsolutePath() + "/tessdata/");

        if (tessDataDirExists()) {
            f.delete();
        }

        if (!f.exists()) {
            TessDownloader td = new TessDownloader();

            try {
                td.execute(mContext.getString(R.string.tessDownloadResource), f.toString()).get();
            } catch (InterruptedException e) {
                Log.e(TAG, String.valueOf(e));
            } catch (ExecutionException e) {
                Log.e(TAG, String.valueOf(e));
            }
        }

        assertTrue(f.exists() && f.isDirectory());
    }


    private Field setUpField(int resource) {
        field = new Field();
        field.x(0);
        field.y(0);
        field.setBitmap(loadBitmap(resource));
        return field;
    }

    public void testDetectOne() {
        grid.setField(setUpField(R.drawable.one));
        assertEquals(1, field.getNumber());
    }

    public void testDetectTwo() {
        grid.setField(setUpField(R.drawable.two));
        assertEquals(2, field.getNumber());
    }

    public void testDetectThree() {
        grid.setField(setUpField(R.drawable.three));
        assertEquals(3, field.getNumber());
    }

    public void testDetectFour() {
        grid.setField(setUpField(R.drawable.four));
        assertEquals(4, field.getNumber());
    }

    public void testDetectFive() {
        grid.setField(setUpField(R.drawable.five));
        assertEquals(5, field.getNumber());
    }

    public void testDetectSix() {
        grid.setField(setUpField(R.drawable.six));
        assertEquals(6, field.getNumber());
    }

    public void testDetectSeven() {
        grid.setField(setUpField(R.drawable.seven));
        assertEquals(7, field.getNumber());
    }

    public void testDetectEight() {
        grid.setField(setUpField(R.drawable.eight));
        assertEquals(8, field.getNumber());
    }

    public void testDetectNine() {
        grid.setField(setUpField(R.drawable.nine));
        assertEquals(9, field.getNumber());
    }

    public void testDetectCross() {
        grid.setField(setUpField(R.drawable.cross));
        assertEquals(0, field.getNumber());
    }

    public void testDetectEmpty() {
        grid.setField(setUpField(R.drawable.empty));
        assertEquals(0, field.getNumber());
    }

    /**
     * Method tests if grid is valid sudoku. Checks how many occurrences of a number exist in each row/column.
     * Adds numbers from row/column to array list, removes duplicates, if size of the array is less than size of the grid,
     * than at least one number exists at least once in a row/column. Sudoku is invalid in this case.
     *
     * @param g Grid to test
     * @return
     */
    private boolean isValid(Grid g) {
        ArrayList<Integer> row;
        Set<Integer> hs = new HashSet<>();

        for (int i = 0; i < g.getSize(); i++) {
            row = new ArrayList<>();
            for (int j = 0; j < g.getSize(); j++) {
                row.add(g.getFieldAt(i, j).getNumber()); // i, j tests rows
            }
            hs.addAll(row); //remove duplicates by puting data to HashSet
            row.clear();
            row.addAll(hs);
            hs.clear();
            if (row.size() < g.getSize())
                return false;
        }

        ArrayList<Integer> col;
        for (int i = 0; i < g.getSize(); i++) {
            col = new ArrayList<>();
            for (int j = 0; j < g.getSize(); j++) {
                col.add(g.getFieldAt(j, i).getNumber()); // j, i tests columns
            }
            hs.addAll(col); //remove duplicates by puting data to HashSet
            col.clear();
            col.addAll(hs);
            hs.clear();
            if (col.size() < g.getSize())
                return false;
        }

        return true; //no duplicates
    }

    public void testValidSolver() {
        size = 3;
        /**
         * 1 0 0
         * 0 1 0
         * 0 0 0
         */
        grid.getFieldAt(0, 0).setNumber(1);
        grid.getFieldAt(1, 1).setNumber(1);

        try {
            new Solver().execute(grid).get();
        } catch (InterruptedException e) {
            Log.e(TAG, String.valueOf(e));
        } catch (ExecutionException e) {
            Log.e(TAG, String.valueOf(e));
        }

        assertTrue(isValid(grid));
    }

    public void testInvalidSolver() {
        boolean isValid; //should be false
        size = 3;
        /**
         * 1 0 0
         * 0 1 0
         * 0 0 0
         */
        grid.getFieldAt(0, 0).setNumber(1);
        grid.getFieldAt(0, 1).setNumber(1);
        isValid = isValid(grid);

        assertFalse(isValid);
    }


    public void testPossibleToSolve3by3() {
        size = 3;
        /**
         * 1 0 0
         * 0 1 0
         * 0 0 0
         */
        grid.getFieldAt(0, 0).setNumber(1);
        grid.getFieldAt(1, 1).setNumber(1);

        boolean possibleToSolve = false;
        try {
            possibleToSolve = new Solver().execute(grid).get();
        } catch (InterruptedException e) {
            Log.e(TAG, String.valueOf(e));
        } catch (ExecutionException e) {
            Log.e(TAG, String.valueOf(e));
        }

        assertTrue(possibleToSolve);
    }

    public void testImpossibleToSolve3by3() {
        size = 3;
        /**
         * 1 0 0 //
         * 1 0 0 //invalid
         * 0 0 0
         */
        grid.getFieldAt(0, 0).setNumber(1);
        grid.getFieldAt(0, 1).setNumber(1);

        boolean possibleToSolve = false;
        try {
            possibleToSolve = new Solver().execute(grid).get();
        } catch (InterruptedException e) {
            Log.e(TAG, String.valueOf(e));
        } catch (ExecutionException e) {
            Log.e(TAG, String.valueOf(e));
        }
        assertFalse(possibleToSolve);
    }

    public void testPossibleToSolve3by3WithInvalidNumber() {
        size = 3;
        /**
         * 1 0 0
         * 0 2 0
         * 0 0 7 //invalid number, will be skipped by solver
         */
        grid.getFieldAt(0, 0).setNumber(1);
        grid.getFieldAt(1, 1).setNumber(2);
        grid.getFieldAt(2, 2).setNumber(7);
        grid.getFieldAt(2, 2).setIsValid(false);

        boolean possibleToSolve = false;
        try {
            possibleToSolve = new Solver().execute(grid).get().booleanValue();
        } catch (InterruptedException e) {
            Log.e(TAG, String.valueOf(e));
        } catch (ExecutionException e) {
            Log.e(TAG, String.valueOf(e));
        }
        assertTrue(possibleToSolve);
    }

    public void testImpossibleToSolve3by3WithInvalidNumber() {
        size = 3;
        /**
         * 1 1 0 //invalid
         * 0 0 0
         * 0 0 7
         */
        grid.getFieldAt(0, 0).setNumber(1);
        grid.getFieldAt(0, 1).setNumber(1);
        grid.getFieldAt(0, 0).setIsValid(true);
        grid.getFieldAt(0, 1).setIsValid(true);

        grid.getFieldAt(2, 2).setNumber(7);
        grid.getFieldAt(2, 2).setIsValid(false);

        boolean possibleToSolve = false;
        try {
            possibleToSolve = new Solver().execute(grid).get();
        } catch (InterruptedException e) {
            Log.e(TAG, String.valueOf(e));
        } catch (ExecutionException e) {
            Log.e(TAG, String.valueOf(e));
        }
        assertFalse(possibleToSolve);
    }

    public void testImpossibleToSolve9by9() {
        size = 9;
        grid.getFieldAt(0, 0).setNumber(3);
        grid.getFieldAt(0, 0).setIsValid(true);
        grid.getFieldAt(0, 1).setNumber(3);
        grid.getFieldAt(0, 1).setIsValid(true);

        boolean possibleToSolve = false;
        try {
            possibleToSolve = new Solver().execute(grid).get();
        } catch (InterruptedException e) {
            Log.e(TAG, String.valueOf(e));
        } catch (ExecutionException e) {
            Log.e(TAG, String.valueOf(e));
        }
        assertFalse(possibleToSolve);
    }

    public void testPossibleToSolve9by9() {
        size = 9;
        grid.getFieldAt(0, 0).setNumber(1);
        grid.getFieldAt(0, 1).setNumber(3);

        boolean possibleToSolve = false;
        try {
            possibleToSolve = new Solver().execute(grid).get();
        } catch (InterruptedException e) {
            Log.e(TAG, String.valueOf(e));
        } catch (ExecutionException e) {
            Log.e(TAG, String.valueOf(e));
        }

        assertTrue(possibleToSolve);
    }

}
